import nacl
from nacl.signing import SigningKey

key = SigningKey.generate()
print(key.encode(encoder=nacl.encoding.HexEncoder))
