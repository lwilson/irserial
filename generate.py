import util
import string
import os

key = input("Secret key: ")

year = int(input("Year (YYYY): "))

for month in range(1,13): # months 1-12
    for letter in list(string.ascii_uppercase): # letters A-Z
        for value in [1, 2, 5, 6, 10]: # normal denominations
            path = '/'.join(["gen",str(year),str(month).zfill(2),letter,str(value).zfill(3)])
            os.makedirs(os.path.dirname(path), exist_ok=True)
            with open(path, 'w') as f:
                for i in range(100): # generate 100 keys
                    num = util.serialize(key, value, year, month, letter, util.genbill())
                    f.write(num + '\n')

