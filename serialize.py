import util

key = input("Secret key: ")

value  = int(input("Value (1, 5, 10, etc): "))
year   = int(input("Year (YYYY): "))
month  = int(input("Month (MM): "))
letter = input("Pinter letter: ").upper()
bill   = input("Bill ID (XXXXX, leave blank to generate): ").upper()
if (bill == ""):
    bill = util.genbill()
    print("Randomized bill ID: " + bill)
print(util.serialize(key, value, year, month, letter, bill))
