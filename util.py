import hashlib
import nacl
from nacl.signing import SigningKey
import random

def hash(input):
    return hashlib.sha512(str(input).encode("utf-8") ).hexdigest().upper()

def genbill():
    return '{:X}'.format(random.getrandbits(20)).zfill(5)

def serialize(key, value, year, month, letter, bill):
    secret = SigningKey(key, encoder=nacl.encoding.HexEncoder)
    data = hash(bill + str(value).zfill(4) + str(year * month * month).zfill(6) + letter + bill)
    signed = str(secret.sign(data.encode('ascii'), encoder=nacl.encoding.HexEncoder)).zfill(5)
    check = signed[-6:-1]
    return bill + check
